module andtest;
reg a,b,c;
wire d;


and and3(d, a, b, c); 

initial
begin
	$display("start test and gate");
	a = 0;
	b = 0;
	c = 0;
end

always #4 begin
	a = a + 1;
end
always #2 begin
	b = b + 1;
end
always #1 begin
	c = c + 1;
end

always @(a or b or c) begin
	$display("%d,%d,%d => %d", a, b, c, d);
end

initial #7 $finish;
endmodule
