`timescale 10ns/1ns
module test_jkff;
reg d = 1;
reg clk = 1'b1;
reg en = 1'b1;
reg rst = 1'b1;
wire q, q_bar;

reg [0:1] cnt = 2'd0;
reg [0:7] cnt2 = 8'd0;

dff dff1(q, q_bar, d, rst, en, clk);
initial begin
	$dumpfile("test_dff.vcd");
        $dumpvars;
end
always #1 begin
	clk = ~clk;
	if(cnt == 0 && cnt2 < 10) begin
		d = ~d;
	end
	cnt = cnt + 1;
end

initial #30 $finish;
endmodule
