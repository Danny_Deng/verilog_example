module xortest;
reg a, b;
wire c;

xor xor1(c, a, b);

initial
	begin
		a = 0;
		b = 0;
	end
always #1 begin
	a = a+1;
end
always #2 begin
	b = b+1;
end	
always @(a or b) begin
	$display("a=%d, b=%d, c=%d", a, b, c);
end
	
initial #3 $finish;
endmodule
