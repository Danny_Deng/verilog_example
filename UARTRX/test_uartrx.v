module test_uartrx;

reg clk = 1'b0;
wire trigger;
reg rx_in;
reg [0:7] testSend;
wire [0:7] testRec;
reg [0:4] cnt = 0;

initial begin
	$dumpfile("test_uart.vcd");
        $dumpvars;
	testSend = 8'b10111010;
	rx_in = 1'b1;
end

uartrx uart1(testRec, rx_in, clk, trigger);

always @(negedge trigger) begin
	$display("data:%h", testRec[0]);
	$display("data:%h", testRec[1]);
	$display("data:%h", testRec[2]);
	$display("data:%h", testRec[3]);
	$display("data:%h", testRec[4]);
	$display("data:%h", testRec[5]);
	$display("data:%h", testRec[6]);
	$display("data:%h", testRec[7]);
end

always #1 begin
	if(cnt == 0) begin
		rx_in <= 1'b0;
	end
	else if(cnt < 9) begin
		rx_in <= testSend[cnt-1];
	end
	else begin
		rx_in <= 1'b1;
		cnt = 0;
	end
	// $display("rx_in:%h, cnt:%h", rx_in, cnt);
	cnt <= cnt + 1;
	clk = ~clk;
end


initial #15 $finish;
endmodule
