module test_jkff;

reg x1 = 1'b1;
reg x2 = 1'b0;
reg s = 1'b0;
wire y;

mul mul1(x1, x2, s, y);

initial begin
	$dumpfile("test_jkff.vcd");
        $dumpvars;
end

always #1 begin
	s = ~s;
end

initial #10 $finish;
endmodule
