module mul(x1, x2, s, y);
// s = 0, y = x1
// s = 1, y = x2
input x1, x2, s;
output y;

assign y = (~s&x1)|(s&x2);

endmodule
