module test_uart;

reg clk = 1'b0;
reg trigger = 1'b0;
wire tx_out;
reg [0:7] testSend;
reg [0:7] testRec;


initial begin
	$dumpfile("test_uart.vcd");
        $dumpvars;
	testSend = 8'b10111010;	
end

uarttx uart1(testSend, tx_out, clk, trigger);

always #1 begin
	clk = ~clk;
	trigger = 1'b0;
	$display("%h, %h", tx_out, clk);
end


initial #15 $finish;
endmodule
