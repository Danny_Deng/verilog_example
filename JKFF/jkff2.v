module jkff2(q, q_bar, j, k, clk);
input j, k, clk;
output q, q_bar;

wire w1, w2;

//reg q_r, q_bar_r;

initial begin 
	force q = 1'b0;
	force q_bar = 1'b1;
end

nand g0(w1, q_bar, j, clk);
nand g1(w2, clk, k, q);
nand g2(q, w1, q_bar);
nand g3(q_bar, w2, q);
/*
always @(posedge clk) begin
	q <= !(w1&&q_bar);
	q_bar <= !(w2&&q);
	// $display("%x, %x, %x", q, q_bar, clk);
end
*/

endmodule
