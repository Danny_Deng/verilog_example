module test_jkff;

reg clk = 0;
reg j1 = 1;
reg k1 = 1;
reg j0 = 0;
reg k0 = 0;
reg pr = 1;
reg cl = 1;
wire q, q_bar;
wire qq, qq_bar;
wire qqq, qqq_bar;
wire qqqq, qqqq_bar;
wire q2;

initial begin
	$dumpfile("test_jkff.vcd");
        $dumpvars;
end

jkff jk1(q, q_bar, j1, k1, clk, pr, cl);
jkff jk2(qq, qq_bar, j1, k0, clk, pr, cl);
jkff jk3(qqq, qqq_bar, j0, k1, clk, pr, cl);
jkff jk4(qqqq, qqqq_bar, j0, k0, clk, pr, cl);

always #1 begin
	clk = ~clk;
	// positive triggle will change the logic.
	$display("================================");
	$display("toggle  Q:%h, Q_bar:%h", q, q_bar);
	$display("J1 K0   Q:%h, Q_bar:%h", qq, qq_bar);
	$display("J0 K1   Q:%h, Q_bar:%h", qqq, qqq_bar);
	$display("Reserve Q:%h, Q_bar:%h", qqqq, qqqq_bar);
	$display("%x, %x", pr, cl);
end

initial #10 $finish;
endmodule
